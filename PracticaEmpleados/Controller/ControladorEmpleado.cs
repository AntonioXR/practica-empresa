using System;
using PracticaEmpleados.Model;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using PracticaEmpleados.View;
using System.Xml.Serialization;
using System.Diagnostics;

namespace PracticaEmpleados.Controller
{
    public class ControladorEmpleado
    {
        #region "Singleton"
        private static ControladorEmpleado instancia;
        public static ControladorEmpleado getInstancia() {
            return (instancia == null) ? (instancia = new ControladorEmpleado()) : instancia;
        }
        #endregion

        #region "Constructor"
        public ControladorEmpleado()
        {
             if (File.Exists(ControladorGeneral.getInstancia().getDatos("pathEmpleado")))
            {
                String dataToArray = File.ReadAllText(ControladorGeneral.getInstancia().getDatos("pathEmpleado"));
                if (ControladorGeneral.getInstancia().getDatos("dot").Equals("XML"))
                {
                    try
                    {
                        var toXML = new XmlSerializer(empleados.GetType());
                        using (StringReader datos = new StringReader(dataToArray)) {

                        empleados = (List<Empleado>)toXML.Deserialize(datos);
                        }
                   }
                    catch (Exception ex) {
                    empleados = JsonConvert.DeserializeObject<List<Empleado>>(dataToArray);
                    }
                }
                else
                {
                    try
                    {
                        empleados = JsonConvert.DeserializeObject<List<Empleado>>(dataToArray);
                   }
                    catch (Exception ex)
                    {
                        var toXML = new XmlSerializer(empleados.GetType());
                        StreamReader reader = new StreamReader(ControladorGeneral.getInstancia().getDatos("pathEmpleado"));
                        empleados = (List<Empleado>)toXML.Deserialize(reader);
                        reader.Close();
                    }
                }
            }
        }
        #endregion

        #region "Propiedades"
        private List<Empleado> empleados = new List<Empleado>();
        private Empleado empleadoTEMP = null;
        public List<Empleado> Empleados {
            get { return empleados; }
        }
        #endregion

        #region "CRUD Empleados"
        public void AgregarEmpleado()
        {
            empleadoTEMP = new Empleado();
            Console.WriteLine("+---------- Ingrese los datos del nuevo empleado -----------+");
            empleadoTEMP.IdEmpleado = empleados.Count + 1;
            Console.Write("Nombre: ");
            empleadoTEMP.NombreEmpleado = Console.ReadLine();
            Console.Write("Apellido: ");
            empleadoTEMP.ApellidoEmpleado = Console.ReadLine();
            Console.Write("Telefono: ");
            empleadoTEMP.TelefonoEmpleado = Console.ReadLine();
            Console.Write("Direccion: ");
            empleadoTEMP.DireccionEmpleado = Console.ReadLine();
            Console.WriteLine("\nElija uno de los departamento, y escriba el ID \n");
            empleadoTEMP.DepartamentoEmpleado = ControladorDepartamento.getInstancia().getDepartamento();
            Console.WriteLine("Se le asigno al departamento de " + empleadoTEMP.DepartamentoEmpleado.NombreDepartamento);
            Console.WriteLine("\nElija uno de los puestos, y escriba el ID \n");
            empleadoTEMP.PuestoEmpleado = ControladorPuesto.getInstancia().getPuesto();
            Console.WriteLine("Se le ha asignado el puesto " + empleadoTEMP.PuestoEmpleado.NombrePuesto);
            Console.Write("Salario: ");
            empleadoTEMP.SalarioEmpleado = Convert.ToDouble(Console.ReadLine());
            empleados.Add(empleadoTEMP);
            toFile();
            Console.WriteLine("+------------ El Empleado fue agregado ---------------+");
            Menus.getInstancia().MenuEmpleados();
        }
        public void MostrarEmpleados()
        {
            Console.WriteLine("########## Estos son los empleados en Nomina ##########");
            foreach (Empleado emp in empleados) {
                Console.WriteLine("----------------------------------------");
                Console.WriteLine("Id: " + emp.IdEmpleado.ToString() + "\nNombre Completo: " + emp.NombreEmpleado + " " + emp.ApellidoEmpleado + "\nTelefono: " + emp.TelefonoEmpleado + " \nDireccion: " + emp.DireccionEmpleado + "\nDepartamento: " + emp.DepartamentoEmpleado.NombreDepartamento + "\nPuesto: " + emp.PuestoEmpleado.NombrePuesto + "\nSalario: " + emp.SalarioEmpleado);
                Console.WriteLine("---------------------------------------");
            }
            Menus.getInstancia().MenuEmpleados();
        }
        public void EditarEmpleados()
        {
            Console.WriteLine("+---------- Ingrese el ID del empleado a editar ----------+");
            Console.Write("ID: ");
            int idEmp = Convert.ToInt32(Console.ReadLine());
            int index = 0;
            for (int i = 0; i < empleados.Count; i++) {
             if(empleados[i].IdEmpleado == idEmp)
                {
                    empleadoTEMP = empleados[i];
                    index = i;
                    break;
                }
            }
            Console.WriteLine("Elij�o el usuario ID: " + empleadoTEMP.IdEmpleado.ToString() + " Nombre: " + empleadoTEMP.NombreEmpleado + " " + empleadoTEMP.ApellidoEmpleado);
            Console.WriteLine("Ingrese los nuevos datos del usuario");
            Console.Write("Nombre: ");
            empleadoTEMP.NombreEmpleado = Console.ReadLine();
            Console.Write("Apellido: ");
            empleadoTEMP.ApellidoEmpleado = Console.ReadLine();
            Console.Write("Telefono: ");
            empleadoTEMP.TelefonoEmpleado = Console.ReadLine();
            Console.Write("Direccion: ");
            empleadoTEMP.DireccionEmpleado = Console.ReadLine();
            Console.WriteLine("\nElija uno de los departamento, y escriba el ID \n");
            empleadoTEMP.DepartamentoEmpleado = ControladorDepartamento.getInstancia().getDepartamento();
            Console.WriteLine("Se le asigno al departamento de " + empleadoTEMP.DepartamentoEmpleado.NombreDepartamento);
            Console.WriteLine("\nElija uno de los puestos, y escriba el ID \n");
            empleadoTEMP.PuestoEmpleado = ControladorPuesto.getInstancia().getPuesto();
            Console.WriteLine("Se le ha asignado el puesto " + empleadoTEMP.PuestoEmpleado.NombrePuesto);
            Console.Write("Salario: ");
            empleadoTEMP.SalarioEmpleado = Convert.ToDouble(Console.ReadLine());
            empleados.RemoveAt(index);
            empleados.Insert(index, empleadoTEMP);
            toFile();
            Console.WriteLine("\n------- Se ha modificado el Empleado ------");
            Menus.getInstancia().MenuEmpleados();
        }
        public void EliminarEmpleado()
        {
            Console.WriteLine("+---------- Ingrese el ID del empleado a eliminar ----------+");
            Console.Write("ID: ");
            int idEmp = Convert.ToInt32(Console.ReadLine());
            int index = 0;
            for (int i = 0; i < empleados.Count; i++)
            {
                if (empleados[i].IdEmpleado == idEmp)
                {
                    empleadoTEMP = empleados[i];
                    index = i;
                    break;
                }
            }
            Console.WriteLine("Elij�o el usuario ID: " + empleadoTEMP.IdEmpleado.ToString() + " Nombre: " + empleadoTEMP.NombreEmpleado + " " + empleadoTEMP.ApellidoEmpleado);
            Console.WriteLine("�Seguro que desea eliminarlo? 1. Si | 2. No");
            Console.Write(">");
            String opcion = Console.ReadLine();
            if (opcion.Equals("1"))
            {
                empleados.RemoveAt(index);
                toFile();
                Console.WriteLine("------- El empleado a sido eliminado -----------");
            }
            else if (opcion.Equals("2"))
            {
                Console.WriteLine("------- La operacion a sido cancelada -----------");
            }
            else {
                Console.WriteLine("---------- ELIJA UNA OPCION VALIDA >:v -----------");
            }
            Menus.getInstancia().MenuEmpleados();
        }
        public void EliminarPorDepto(int idDepto)
        {
            List<Empleado> ids = new List<Empleado>();
            foreach (Empleado emp in empleados)
            {
                if (emp.DepartamentoEmpleado.IdDepartamento == idDepto)
                {
                    ids.Add(emp);
                }
            }
            foreach (Empleado emp in ids)
            {
               empleados.Remove(emp);
            }
        }
        public void EliminarPorPuesto(int idPuesto)
        {
            List<Empleado> ids = new List<Empleado>();
            foreach (Empleado emp in empleados)
            {
                if (emp.DepartamentoEmpleado.IdDepartamento == idPuesto)
                {
                    ids.Add(emp);
                }
            }
            foreach (Empleado emp in ids)
            {
               empleados.Remove(emp);
            }
        }
        #endregion

        #region "Buscador"
        public void BuscadorEmpleado()
        {
            String datos= "";
            Boolean realizado = false;
            try
            {
                Console.WriteLine("Ingrese el ID o Nombre o Apellido del empleado");
                Console.Write(">");
                datos = Console.ReadLine();
                int id = Convert.ToInt32(datos);
                foreach (Empleado emp in empleados) {
                    if (emp.IdEmpleado == id) {
                        Console.WriteLine("\n+------------------------------------------+");
                        Console.WriteLine("ID: "+emp.IdEmpleado.ToString()+" \nNombre: "+emp.NombreEmpleado+"\nApellido: "+emp.ApellidoEmpleado+"\nDireccion: "+emp.DireccionEmpleado+"\nTelefono: "+emp.TelefonoEmpleado+"\nPuesto: "+emp.PuestoEmpleado.NombrePuesto+"\nDepartamento: "+emp.DepartamentoEmpleado.NombreDepartamento+"\nSalario: "+emp.SalarioEmpleado.ToString());
                        Console.WriteLine("+------------------------------------------+\n");
                        realizado = true;
                    }
                }
            } catch (Exception ex) {
                foreach (Empleado emp in empleados) {
                    if (emp.NombreEmpleado.Contains(datos) || emp.ApellidoEmpleado.Contains(datos)){
                        Console.WriteLine("\n+------------------------------------------+");
                        Console.WriteLine("ID: " + emp.IdEmpleado.ToString() + " \nNombre: " + emp.NombreEmpleado + "\nApellido: " + emp.ApellidoEmpleado + "\nDireccion: " + emp.DireccionEmpleado + "\nTelefono: " + emp.TelefonoEmpleado + "\nPuesto: " + emp.PuestoEmpleado.NombrePuesto + "\nDepartamento: " + emp.DepartamentoEmpleado.NombreDepartamento + "\nSalario: " + emp.SalarioEmpleado.ToString());
                        Console.WriteLine("+------------------------------------------+\n");
                        realizado = true;
                    }
                }

            }
            if (!realizado) {
                Console.WriteLine("\n+------------------------------------------+");
                Console.WriteLine("\n### NO SE ENCONTRO NINGUNA COINCIDENCIA ###\n");
                Console.WriteLine("+------------------------------------------+\n");
            }
            Menus.getInstancia().MenuEmpleados();
        }
        #endregion

        #region "Serializar/Deserializar"
        public void toFile()
        {
            if (ControladorGeneral.getInstancia().getDatos("dot").Equals("XML"))
            {
                var XML = new StringWriter();
                var toXML = new XmlSerializer(empleados.GetType());
                toXML.Serialize(XML, empleados);
                toFileSave(XML.ToString());
            }
            else
            {
                toFileSave(JsonConvert.SerializeObject(empleados));
            }
        }

        public void toFileSave(String dataToSave)
        {
            if (File.Exists(ControladorGeneral.getInstancia().getDatos("pathEmpleado")))
            {
                FileStream data = File.Open(ControladorGeneral.getInstancia().getDatos("pathEmpleado"), FileMode.Open);
                Byte[] datos = new UTF8Encoding(true).GetBytes(dataToSave);
                data.Write(datos, 0, datos.Length);
                data.Close();
            }
            else
            {
                FileStream data = File.Create(ControladorGeneral.getInstancia().getDatos("pathEmpleado"));
                Byte[] datos = new UTF8Encoding(true).GetBytes(dataToSave);
                data.Write(datos, 0, datos.Length);
                data.Close();
            }
        }

        #endregion
    }
}
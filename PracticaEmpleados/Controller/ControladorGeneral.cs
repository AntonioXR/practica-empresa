﻿using System;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using System.Collections.Generic;


using PracticaEmpleados.View;

namespace PracticaEmpleados.Controller
{
    public class ControladorGeneral
    {
        /* 
         Esta clase guardara datos de la aplicacion, como si sera JSON o XML direccion del path de cada clase
         pathPuesto: configPuestos.json
         pathDepartamento : configDepartamentos.json
         pathEmpleado : configEmpleados.json
         dot : json -Como inicio se indicara que se hara con JSON la serializacion
             */

        #region "Constructor"
        public ControladorGeneral()
        {
            if (File.Exists("config.json"))
            {
                String JSON = File.ReadAllText("config.json");
                datos = JsonConvert.DeserializeObject<Dictionary<String, String>>(JSON);
            }
        }
        #endregion

        #region "Singleton"
        private static ControladorGeneral instancia;
        public static ControladorGeneral getInstancia()
        {
            return (instancia == null) ? (instancia = new ControladorGeneral()) : instancia;
        }
        #endregion

        #region "Propiedades"
        Dictionary<String, String> datos = new Dictionary<string, string>();
        #endregion

        #region "Ingreso y obtencion de datos"
        public void setDatos(String key, String contenido)
        {
            datos[key] = contenido;
            toFile();
        }
        public String getDatos(String key) {
            return datos[key];
        }
        #endregion

        #region "Menu de config"
        public void configuraciones()
        {
           
            Console.WriteLine(" ------- Bienvenido a las configuraciones -------");
            Console.WriteLine("1.Configurar direccion de archivo de Departamentos\n2.Configurar direccion de archivo de Puesto\n3.Configurar direccion de archivo de Empleado\n\t4. JSON | XML\n\t5.Volver al menu inicial");
            String opcion = Console.ReadLine();
            switch (opcion) {
                case "1":
                    Console.WriteLine("Actualmente se Guardan en " + getDatos("pathDepartamento"));
                    Console.WriteLine("Precione ENTER y elija donde estara el nuevo archivo");
                    Console.ReadKey();
                    Console.WriteLine("Escriba donde estara el nuevo archivo para los Departamentos");
                    Console.Write(">");
                    String newPath= Console.ReadLine();
                    Console.WriteLine("¿Seguro que el nuevo archivo se encontrara en "+newPath+" ? \n1. Si | 2. No");
                    String option = Console.ReadLine();
                    if(option.Equals("1"))
                    {
                        setDatos("pathDepartamento",newPath);
                        Console.WriteLine("Se a cambiado la direccion del archivo para los Departamentos");
                    }else if(option.Equals("2"))
                    {
                        Console.WriteLine("No se ha modificado la direccion");
                    }
                    break;
                case "2":
                    Console.WriteLine("Actualmente se Guardan en " + getDatos("pathPuesto"));
                    Console.WriteLine("Escriba donde estara el nuevo archivo para los puestos");
                    Console.Write(">");
                    String newPathP= Console.ReadLine();
                    Console.WriteLine("¿Seguro que el nuevo archivo se encontrara en "+ newPathP + " ? \n1. Si | 2. No");
                    String optionP = Console.ReadLine();
                    if(optionP.Equals("1"))
                    {
                        setDatos("pathPuesto", newPathP);
                        Console.WriteLine("Se a cambiado la direccion del archivo para los puestos");
                    }else if(optionP.Equals("2"))
                    {
                        Console.WriteLine("No se ha modificado la direccion");
                    }
                    break;
                case "3":
                    Console.WriteLine("Actualmente se Guardan en " + getDatos("pathEmpleado"));
                    Console.WriteLine("Escriba donde estara el nuevo archivo de Empleados");
                    Console.Write(">");
                    String newPathE= Console.ReadLine();
                    Console.WriteLine("¿Seguro que el nuevo archivo se encontrara en "+ newPathE + " ? \n1. Si | 2. No");
                    String optionE = Console.ReadLine();
                    if(optionE.Equals("1"))
                    {
                        setDatos("pathEmpleado", newPathE);
                        Console.WriteLine("Se a cambiado la direccion del archivo de empleados");
                    }else if(optionE.Equals("2"))
                    {
                        Console.WriteLine("No se ha modificado la direccion");
                    }
                    break;
                case "4":
                    Console.WriteLine("Actualmente se Guardan en archivos de tipo "+getDatos("dot"));
                    Console.WriteLine("Se guardaran los archivos mediante XML o JSON\n\t1. XML | 2. JSON");
                    Console.Write(">");
                    String dot = Console.ReadLine();
                    Console.WriteLine("¿Seguro que se almacenara en un archivo de tipo " + (dot.Equals("1")? "XML": "JSON") + " ? \n1. Si | 2. No");
                    String OPD = Console.ReadLine();
                    if (OPD.Equals("1"))
                    {
                        setDatos("dot", "XML");
                        Console.WriteLine("Se a cambiado XML");
                    }
                    else if (OPD.Equals("2"))
                    {
                        setDatos("dot", "JSON");
                        Console.WriteLine("Se a cambiado a JSON");
                    }
                    break;
                case "5":

                    break;
            }
            Menus.getInstancia().MenuMain();
        }
        #endregion

        #region "Serializar"
        public void toFile()
        {
            if (File.Exists("config.json"))
            {
                FileStream conf = File.Open("config.json", FileMode.Open);
                Byte[] data = new UTF8Encoding(true).GetBytes(JsonConvert.SerializeObject(datos));
                conf.Write(data, 0, data.Length);
                conf.Close();
            }
            else
            {
                FileStream conf = File.Create("config.json");
                Byte[] data = new UTF8Encoding(true).GetBytes(JsonConvert.SerializeObject(datos));
                conf.Write(data, 0, data.Length);
                conf.Close();
            }
        }
        #endregion
    }
}

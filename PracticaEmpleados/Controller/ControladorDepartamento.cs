﻿using System;
using PracticaEmpleados.Model;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using PracticaEmpleados.View;
using System.Xml.Serialization;

namespace PracticaEmpleados.Controller
{
    public class ControladorDepartamento
    {
        #region "Singleton"
        private static ControladorDepartamento instancia;
        public static ControladorDepartamento getInstancia()
        {
            return (instancia == null) ?(instancia = new ControladorDepartamento()) :instancia ;
        }
        #endregion

        #region "Constructor"
        public ControladorDepartamento() {
            if (File.Exists(ControladorGeneral.getInstancia().getDatos("pathDepartamento")))
            {
                String dataToArray = File.ReadAllText(ControladorGeneral.getInstancia().getDatos("pathDepartamento"));
                if (ControladorGeneral.getInstancia().getDatos("dot").Equals("XML"))
                {
                    try
                    {
                        var toXML = new XmlSerializer(departamentos.GetType());
                        using (StringReader reader = new StringReader(dataToArray)) {
                            departamentos = (List<Departamento>)toXML.Deserialize(reader);

                        }
                        
                    } catch (Exception ex) {
                        departamentos = JsonConvert.DeserializeObject<List<Departamento>>(dataToArray);
                    }
                }
                else
                {
                    try
                    { departamentos = JsonConvert.DeserializeObject<List<Departamento>>(dataToArray);
                    } catch (Exception ex) {
                        var toXML = new XmlSerializer(departamentos.GetType());
                        StreamReader reader = new StreamReader(ControladorGeneral.getInstancia().getDatos("pathDepartamento"));
                        departamentos = (List<Departamento>)toXML.Deserialize(reader);
                        reader.Close();
                    }
                   
                }
            }


        }
        #endregion

        #region "Propiedades"
        private List<Departamento> departamentos = new List<Departamento>();
        private Departamento departamentoTEMP;
        public List<Departamento> Departamentos
        {
            get { return departamentos; }
        }
        #endregion

        #region"CRUD Departamento"

        public void MostrarDepartamentos()
        {
            Console.WriteLine("\n####### Estos son los departamentos disponibles #######");
            foreach (Departamento dep in departamentos)
            {
                Console.WriteLine("> ID: "+dep.IdDepartamento.ToString() +" Nombre del Departamento: "+ dep.NombreDepartamento+" \t<");
            }
            Console.WriteLine("#######################################################");
            Menus.getInstancia().menuDepto();
        }
        public void AgregarDepartamentos()
        {
            departamentoTEMP = new Departamento();
            Console.WriteLine("\n+----Escriba el nombre del nuevo Departamento----+");
            Console.Write(" Nombre del departamento: ");
            departamentoTEMP.NombreDepartamento = Console.ReadLine();
            departamentoTEMP.IdDepartamento = departamentos.Count +1;
            departamentos.Add(departamentoTEMP);
            toFile();
            Console.WriteLine("\n+------ Nuevo departamento guardado ------+");
            Menus.getInstancia().menuDepto();
        }
        public void EditarDepartamento()
        {
            Console.WriteLine("\n+----Escriba el ID del Departamento a modificar----+");
            Console.Write("ID: ");
            String idEdit = Console.ReadLine();
            Departamento depTEMP = null;
            int index = 0;
            for (int i = 0; i < departamentos.Count; i++) {
                if (departamentos[i].IdDepartamento.ToString().Equals(idEdit))
                {
                    depTEMP = departamentos[i];
                    index = i;
                    Console.WriteLine("  Se ha elegido el Departamento de ID: " + depTEMP.IdDepartamento.ToString() + " Nombre: " + depTEMP.NombreDepartamento);
                    break;
                }
            }
            if (depTEMP != null)
            {
            Console.WriteLine("----Escriba el nuevo nombre del departamento----");
            Console.Write("Nombre: ");
            depTEMP.NombreDepartamento = Console.ReadLine();
            departamentos.RemoveAt(index);
            departamentos.Insert(index, depTEMP);
            Console.WriteLine("----El Departamento ha sido modificado----");
            toFile();
            }
            else
            {
                Console.WriteLine("-----ID NO ENCONTRADO VUELVA A VERIFICAR QUE SEA CORRECTO-----");
            }
            Menus.getInstancia().menuDepto();
        }
        public void EliminarDepartamento() {
            Console.WriteLine("\n+----Escriba el ID del Departamento a eliminar----+");
            Console.Write("ID: ");
            String idEdit = Console.ReadLine();
            int index = 0;
            Departamento depTEMP = null;
            for (int i = 0; i < departamentos.Count; i++)
            {
                if (departamentos[i].IdDepartamento.ToString().Equals(idEdit))
                {
                    depTEMP = departamentos[i];
                    index = i;
                    Console.WriteLine("  Se ha elegido el Departamento de ID: " + depTEMP.IdDepartamento.ToString() + " Nombre: " + depTEMP.NombreDepartamento);
                    break;
                }
            }
            if (depTEMP != null)
            {
                Console.WriteLine("----¿DECEA ELIMINAR ESTE DEPARTAMENTO?, AL ELIMINARLO TAMBIEN ELIMINARA LOS EMPLEADOS QUE ESTE EN EL----");
                Console.Write("1. Si | 2. No ");
                if (Console.ReadLine().Equals("1"))
                {
                    departamentos.RemoveAt(index);
                    ControladorEmpleado.getInstancia().EliminarPorDepto(Convert.ToInt32(idEdit));
                    Console.WriteLine("----El Departamento ha sido ELIMINADO----");
                }
                else if (Console.ReadLine().Equals("2"))
                {
                    Console.WriteLine("----La Operacion a sido cancelada----");
                }
                else {
                    Console.WriteLine("---- Opcion no encontrada >:v ----");
                }
                toFile();
            }
            else
            {
                Console.WriteLine("-----ID NO ENCONTRADO VUELVA A VERIFICAR QUE SEA CORRECTO-----");
                Console.ReadKey();
            }
            Menus.getInstancia().menuDepto();
        }
        public Departamento getDepartamento()
        {
            Console.WriteLine("\n####### Estos son los departamentos disponibles #######");
            foreach (Departamento dep in departamentos)
            {
                Console.WriteLine("> ID: " + dep.IdDepartamento.ToString() + " Nombre del Departamento: " + dep.NombreDepartamento + " \t<");
            }
            Console.WriteLine("#######################################################");
            Console.Write("ID: ");
            int idDepartamento = Convert.ToInt32(Console.ReadLine());
            foreach (Departamento depto in departamentos) {
                if (depto.IdDepartamento == idDepartamento)
                {
                    departamentoTEMP = depto;
                    break;
                }
            }
            return departamentoTEMP;
        }
        public void buscarPorDept()
        {
            Console.WriteLine("Ingrese el id del departamento");
            Console.Write(">");
            String id = Console.ReadLine();
            foreach (Departamento dep in departamentos)
            {
                if (dep.IdDepartamento.ToString().Equals(id)) {
                    Console.WriteLine("A seleccionado el Departamento " + dep.NombreDepartamento);
                    break;
                }
            }
            foreach (Empleado emp in ControladorEmpleado.getInstancia().Empleados)
            {
                if (emp.DepartamentoEmpleado.IdDepartamento.ToString().Equals(id))
                {
                    Console.WriteLine("\n+------------------------------------------+");
                    Console.WriteLine("ID: " + emp.IdEmpleado.ToString() + " \nNombre: " + emp.NombreEmpleado + "\nApellido: " + emp.ApellidoEmpleado + "\nDireccion: " + emp.DireccionEmpleado + "\nTelefono: " + emp.TelefonoEmpleado + "\nPuesto: " + emp.PuestoEmpleado.NombrePuesto + "\nDepartamento: " + emp.DepartamentoEmpleado.NombreDepartamento + "\nSalario: " + emp.SalarioEmpleado.ToString());
                    Console.WriteLine("+------------------------------------------+\n");
                }
            }
            Menus.getInstancia().menuDepto();
        }
        #endregion

        #region "Serializar/Deserializar"
        public void toFile()
        {
            if (ControladorGeneral.getInstancia().getDatos("dot").Equals("XML"))
            {
                var XML = new StringWriter();
                var toXML = new XmlSerializer(departamentos.GetType());
                toXML.Serialize(XML, departamentos);
                toFileSave(XML.ToString());
            }
            else
            {
                toFileSave(JsonConvert.SerializeObject(departamentos));
            }
        }

        public void toFileSave(String dataToSave)
        {
            if (File.Exists(ControladorGeneral.getInstancia().getDatos("pathDepartamento")))
            {
                FileStream data = File.Open(ControladorGeneral.getInstancia().getDatos("pathDepartamento"), FileMode.Open);
                Byte[] datos = new UTF8Encoding(true).GetBytes(dataToSave);
                data.Write(datos, 0, datos.Length);
                data.Close();
            }
            else
            {
                FileStream data = File.Create(ControladorGeneral.getInstancia().getDatos("pathDepartamento"));
                Byte[] datos = new UTF8Encoding(true).GetBytes(dataToSave);
                data.Write(datos, 0, datos.Length);
                data.Close();
            }
        }

        #endregion
    }
}

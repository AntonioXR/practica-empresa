﻿using System;
using PracticaEmpleados.View;
using System.Text;
using Newtonsoft.Json;
using System.Xml.Serialization;
using System.IO;
using System.Collections.Generic;
using PracticaEmpleados.Model;

namespace PracticaEmpleados.Controller
{
    public class ControladorPlanilla
    {
        #region "Propiedad"
        private List<Planilla> planillas = new List<Planilla>();
        private Planilla planillaTEMP;
        #endregion

        #region "Singleton"
        private static ControladorPlanilla instancia;
        public static ControladorPlanilla getInstancia()
        {
            return (instancia == null) ? (instancia = new ControladorPlanilla()) : instancia;
        }
        #endregion

        #region "Constructor"
        public ControladorPlanilla()
        {
            if (File.Exists(ControladorGeneral.getInstancia().getDatos("pathPlanilla"))) {
                String dataToArray = File.ReadAllText(ControladorGeneral.getInstancia().getDatos("pathPlanilla"));
                if (ControladorGeneral.getInstancia().getDatos("dot").Equals("XML"))
                {
                    try
                    {
                        var toXML = new XmlSerializer(planillas.GetType());
                        StreamReader reader = new StreamReader(ControladorGeneral.getInstancia().getDatos("pathPlanilla"));
                        planillas = (List<Planilla>)toXML.Deserialize(reader);
                        reader.Close();
                    }
                    catch (Exception ex)
                    {
                        planillas = JsonConvert.DeserializeObject<List<Planilla>>(dataToArray);
                    }
                }
                else
                {
                    try
                    {
                        planillas = JsonConvert.DeserializeObject<List<Planilla>>(dataToArray);
                    }
                    catch (Exception ex)
                    {
                        var toXML = new XmlSerializer(planillas.GetType());
                        StreamReader reader = new StreamReader(ControladorGeneral.getInstancia().getDatos("pathPlanilla"));
                        planillas = (List<Planilla>)toXML.Deserialize(reader);
                        reader.Close();
                    }
                }
            }
        }
        #endregion

        #region "Agregar Planilla"
        public void AgregarPlanilla()
        {
            planillas.Clear();
            Planilla pl = new Planilla();
            foreach (Empleado emp in ControladorEmpleado.getInstancia().Empleados) {
            pl.empleado = emp;
                pl.IGGS = emp.SalarioEmpleado * (0.045);
                pl.Bonificacion = 250.00;
                pl.SeptDia = (emp.SalarioEmpleado / 30) *4;
                pl.SalarioFinal = emp.SalarioEmpleado - pl.IGGS + pl.Bonificacion + pl.SeptDia;
                planillas.Add(pl);
                Console.WriteLine("\n--------------------------------------------------------");
                Console.WriteLine("Nombre: "+emp.NombreEmpleado+" "+emp.ApellidoEmpleado+ "\nDepartamento:"+emp.DepartamentoEmpleado.NombreDepartamento+ "\nPuesto: "+emp.PuestoEmpleado.NombrePuesto + "  \nSalario Base: "+emp.SalarioEmpleado.ToString());
                Console.WriteLine("Cantidad en IGSS: "+pl.IGGS+"\nBonificacion: "+pl.Bonificacion+ "\nSeptimo Dia: "+pl.SeptDia+ "\nSalario Final: "+pl.SalarioFinal);
                Console.WriteLine("----------------------------------------------------------");
                toFile();
            }
            Console.WriteLine("\n----- Estos son todos los empleados que se tienen en la planilla -----");
            Menus.getInstancia().MenuMain();
        }
        #endregion

        #region "Serializar/Deserializar"
        public void toFile()
        {
            if (ControladorGeneral.getInstancia().getDatos("dot").Equals("XML"))
            {
                var XML = new StringWriter();
                var toXML = new XmlSerializer(planillas.GetType());
                toXML.Serialize(XML, planillas);
                toFileSave(XML.ToString());
            }
            else
            {
                toFileSave(JsonConvert.SerializeObject(planillas));
            }
        }

        public void toFileSave(String dataToSave)
        {
            if (File.Exists(ControladorGeneral.getInstancia().getDatos("pathPlanilla")))
            {
                FileStream data = File.Open(ControladorGeneral.getInstancia().getDatos("pathPlanilla"), FileMode.Open);
                Byte[] datos = new UTF8Encoding(true).GetBytes(dataToSave);
                data.Write(datos, 0, datos.Length);
                data.Close();
            }
            else
            {
                FileStream data = File.Create(ControladorGeneral.getInstancia().getDatos("pathPlanilla"));
                Byte[] datos = new UTF8Encoding(true).GetBytes(dataToSave);
                data.Write(datos, 0, datos.Length);
                data.Close();
            }
        }

        #endregion
    }
}

﻿using System;
using PracticaEmpleados.Model;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using PracticaEmpleados.View;
using System.Xml.Serialization;

namespace PracticaEmpleados.Controller
{
    public class ControladorPuesto
    {
        #region "Propiedades"
        private List<Puesto> puestos = new List<Puesto>();
        private Puesto puestoTEMP;

        public List<Puesto> Puesto {
            get { return puestos; }
        }
        #endregion

        #region "Singleton"
        private static ControladorPuesto instancia;
        public static ControladorPuesto getInstancia()
        {
            return (instancia == null) ?(instancia = new ControladorPuesto()) :instancia ;
        }
        #endregion

        #region "Constructor"
        public ControladorPuesto()
        {
            if (File.Exists(ControladorGeneral.getInstancia().getDatos("pathPuesto"))) {
                String dataToArray = File.ReadAllText(ControladorGeneral.getInstancia().getDatos("pathPuesto"));
                if (ControladorGeneral.getInstancia().getDatos("dot").Equals("XML"))
                {
                    try
                    {
                        var toXML = new XmlSerializer(puestos.GetType());
                        using (StringReader texto = new StringReader(dataToArray)) {

                        puestos = (List<Puesto>) toXML.Deserialize(texto);
                        }
                     }
                    catch (Exception ex) {
                        Console.Write(ex.Message);
                        puestos = JsonConvert.DeserializeObject<List<Puesto>>(dataToArray);
                    }
                }
                else {
                    try
                    {
                        puestos = JsonConvert.DeserializeObject<List<Puesto>>(dataToArray);
                    } catch (Exception ex) {
                        var toXML = new XmlSerializer(puestos.GetType());
                        StreamReader reader = new StreamReader(ControladorGeneral.getInstancia().getDatos("pathPuesto"));
                        reader.Close();
                        puestos = (List<Puesto>)toXML.Deserialize(reader);
                    }
                }
            }
        }
        #endregion

        #region "CRUD Puesto"

        public void AgregarPuestos()
        {
            puestoTEMP = new Puesto();
            Console.WriteLine("\n+--------Ingrese el Nombre del Puesto--------+");
            Console.Write("Nombre del Puesto: ");
            puestoTEMP.NombrePuesto = Console.ReadLine();
            puestoTEMP.IdPuesto = puestos.Count + 1;
            puestos.Add(puestoTEMP);
            toFile();
            Console.WriteLine("\n+----------- Puesto agregado ----------------+");
            Menus.getInstancia().menuPuesto();
        }
        public void EditarPuesto()
        {
            Console.WriteLine("\n-------- Ingrese el ID del Puesto a modificar --------");
            Console.Write("ID: ");
            String id = Console.ReadLine();
            Puesto pstd = null;
            int index = 0;
            for (int i = 0; i < puestos.Count; i++) {
                if (puestos[i].IdPuesto.ToString().Equals(id))
                {
                    index = i;
                    Console.WriteLine("> ha elegido el puesto ID: " + puestos[i].IdPuesto + " Puesto: " + puestos[i].NombrePuesto);
                    pstd = puestos[i];
                    break;
                }
            }
            if (pstd != null)
            {
                Console.WriteLine("\n--- Ingrese el nuevo nombre ---");
                Console.Write("Nuevo Nombre: ");
                pstd.NombrePuesto = Console.ReadLine();
                puestos.RemoveAt(index);
                puestos.Insert(index, pstd);
                toFile();
                Console.WriteLine("--- El puesto fue modificado ---");
            }
            else {
                Console.WriteLine("--- NO FUE ENCONTRADO EL PUESTO, INGRESE UN ID VALIDO ---");
            }
            Menus.getInstancia().menuPuesto();
        }
        public void EliminarPuesto()
        {
            Console.WriteLine("\n-------- Ingrese el ID del Puesto a eliminar --------");
            Console.Write("ID: ");
            String id = Console.ReadLine();
            Puesto pstd = null;
            int index = 0;
            for (int i = 0; i < puestos.Count; i++)
            {
                if (puestos[i].IdPuesto.ToString().Equals(id))
                {
                    index = i;
                    Console.WriteLine("> ha elegido el puesto ID: " + puestos[i].IdPuesto + " Puesto: " + puestos[i].NombrePuesto);
                    pstd = puestos[i];
                    break;
                }
            }
            if (pstd != null)
            {
                Console.WriteLine("\n--- DECEA ELIMINAR ESTE PUESTO, TODOS LOS EMPLEADOS DE ESTE PUESTO TAMBIEN SERAN ELIMINADOS ---");
                Console.Write("1. Si | 2. No : ");
                String opcion2 = Console.ReadLine();
                if (opcion2.Equals("1"))
                {

                puestos.RemoveAt(index);
                    ControladorEmpleado.getInstancia().EliminarPorPuesto(Convert.ToInt32(id));
                    Console.WriteLine("--- El puesto a sido eliminado exitosamente ---");
                }
                else if (opcion2.Equals("2"))
                {
                    Console.WriteLine("--- La operacion a sido cancelada ---");
                }
                else {
                    Console.WriteLine("--- INGRESE UNA OPCION VALIDA >:v ---");
                }
                toFile();
                
            }
            else
            {
                Console.WriteLine("--- NO FUE ENCONTRADO EL PUESTO, INGRESE UN ID VALIDO ---");
            }
            Menus.getInstancia().menuPuesto();
        }
        public void MostrarPuestos()
        {
            Console.WriteLine("\n####### Estos son los puestos disponibles #######");
            foreach (Puesto pst in puestos) {
                Console.WriteLine("\n> ID: " + pst.IdPuesto.ToString()+" Puesto: "+pst.NombrePuesto+"\t<");
            }
            Console.WriteLine("\n#################################################");
            Menus.getInstancia().menuPuesto();

        }
        public Puesto getPuesto()
        {
            Console.WriteLine("\n####### Estos son los puestos disponibles #######");
            foreach (Puesto pst in puestos)
            {
                Console.WriteLine("\n> ID: " + pst.IdPuesto.ToString() + " Puesto: " + pst.NombrePuesto + "\t<");
            }
            Console.WriteLine("\n#################################################");
            Console.Write("ID: ");
            int idPuesto = Convert.ToInt32(Console.ReadLine());
            foreach (Puesto pust in puestos) {
                if (pust.IdPuesto == idPuesto)
                {
                    puestoTEMP = pust;
                    break;
                }
            }
            return puestoTEMP;
        }
        #endregion

        #region "Serializar/Deserializar"
        public void toFile() {
            if (ControladorGeneral.getInstancia().getDatos("dot").Equals("XML"))
            {
                var XML = new StringWriter();
                var toXML = new XmlSerializer(puestos.GetType());
                toXML.Serialize(XML, puestos);
                toFileSave(XML.ToString());
            }
            else {
                toFileSave(JsonConvert.SerializeObject(puestos));
            }
        }

        public void toFileSave(String dataToSave)
        {
            if (File.Exists(ControladorGeneral.getInstancia().getDatos("pathPuesto")))
            {
                FileStream data = File.Open(ControladorGeneral.getInstancia().getDatos("pathPuesto"), FileMode.Open);
                Byte[] datos = new UTF8Encoding(true).GetBytes(dataToSave);
                data.Write(datos, 0, datos.Length);
                data.Close();
            }
            else
            {
                FileStream data = File.Create(ControladorGeneral.getInstancia().getDatos("pathPuesto"));
                Byte[] datos = new UTF8Encoding(true).GetBytes(dataToSave);
                data.Write(datos, 0, datos.Length);
                data.Close();
            }
        }
        
        #endregion
    }
}

﻿using System;
using PracticaEmpleados.Controller;
namespace PracticaEmpleados.View
{
    public class Menus
    {
        #region "Singleton"
        private static Menus instancia;
        public static Menus getInstancia()
        {
            return (instancia == null) ?(instancia = new Menus()) : instancia;
        }
        #endregion
        #region "Menus"
        public  void MenuMain()
        {
            String opcion = "";
            while (!opcion.Equals("4")){
            Console.WriteLine("\n+-----------------------------------------------+");
            Console.WriteLine("+\t\t\t\t\t\t+");
            DateTime ahora = DateTime.Now;
            String saludo = (ahora.Hour < 12) ? "Buenos diás " : "Buenas tardes ";
            Console.WriteLine("+\t  " + saludo + " Bienvenido\t\t+");
            Console.WriteLine("+\t   al gestionador de Empleados\t\t+");
            Console.WriteLine("+\t   Elija que decea gestionar\t\t+");
            Console.WriteLine("+\t  1.Departamentos 2.Puestos 3.Empleados +");
            Console.WriteLine("+\t      4.Configuraciones   5.Resumen     +");
            Console.WriteLine("+\t      6.Planilla      7.Salir           +");
            Console.WriteLine("+-----------------------------------------------+");
            Console.Write(">");
             opcion = Console.ReadLine();
            switch (opcion)
            {
                case "1":
                    menuDepto();
                    break;
                case "2":
                        menuPuesto();
                    break;
                case "3":
                        MenuEmpleados();
                    break;
                case "7":
                        Console.WriteLine("........................................");
                        Console.WriteLine(".....#......####...#...####.......###...");
                        Console.WriteLine("....#.#.....#...#..#..#.....#....#...#..");
                        Console.WriteLine("...#...#....#....#.#.#.......#....#.....");
                        Console.WriteLine("..#######...#....#.#.#.......#.....#....");
                        Console.WriteLine(".#.......#..#...#..#..#.....#....#...#..");
                        Console.WriteLine("#.........#.####...#...#.##.......###...");
                        Console.WriteLine("........................................");
                        Console.WriteLine("\nAdios c: gracias por usarme xdxddxdxd ");
                        Console.ReadKey();
                        Environment.Exit(0);
                        break;
                case "4":
                        ControladorGeneral.getInstancia().configuraciones();
                   break;
                case "5":
                   ResumenAll.getInstancia().resumen();
                   break;
                case "6":
                        ControladorPlanilla.getInstancia().AgregarPlanilla();
                   break;
                    default:
                 Console.WriteLine("\n#### ELIJA UNA OPCION VALIDA >:v ####");
                   break;
            }
            }
        }
        public void menuDepto()
        {
            Console.WriteLine("\n+-------------------------------------------------------+");
            Console.WriteLine("+\t\t       DEPARTAMENTOS    \t\t+");
            Console.WriteLine("+  1.Agregar 2.Editar 3.Eliminar 4.Mostrar Disponibles  +");
            Console.WriteLine("+    5.Volver al menu inicial  6.Buscar por depto       +");
            Console.WriteLine("+-------------------------------------------------------+");
            Console.Write(">");
            String opcion2 = Console.ReadLine();

            switch (opcion2)
            {
                case "1":
                    ControladorDepartamento.getInstancia().AgregarDepartamentos();
                    break;
                case "2":
                    ControladorDepartamento.getInstancia().EditarDepartamento();
                    break;
                case "3":
                    ControladorDepartamento.getInstancia().EliminarDepartamento();
                    break;
                case "4":
                    ControladorDepartamento.getInstancia().MostrarDepartamentos();
                    break;
                case "5":
                    MenuMain();
                    break;
                case "6":
                    ControladorDepartamento.getInstancia().buscarPorDept();
                    break;
            }
        }
        public void menuPuesto()
        {
            Console.WriteLine("\n+-------------------------------------------------------+");
            Console.WriteLine("+\t\t         PUESTOS        \t\t+");
            Console.WriteLine("+  1.Agregar 2.Editar 3.Eliminar 4.Mostrar Disponibles  +");
            Console.WriteLine("+                 5.Volver al menu inicial              +");
            Console.WriteLine("+-------------------------------------------------------+");
            Console.Write(">");
            String opcion2 = Console.ReadLine();
            switch (opcion2)
            {
                case "1":
                    ControladorPuesto.getInstancia().AgregarPuestos();
                    break;
                case "2":
                    ControladorPuesto.getInstancia().EditarPuesto();
                    break;
                case "3":
                    ControladorPuesto.getInstancia().EliminarPuesto();
                    break;
                case "4":
                    ControladorPuesto.getInstancia().MostrarPuestos();
                    break;
                case "5":
                    MenuMain();
                    break;
            }
        }
        public void MenuEmpleados()
        {
            Console.WriteLine("\n+-------------------------------------------------------+");
            Console.WriteLine("+\t\t        EMPLEADOS       \t\t+");
            Console.WriteLine("+  1.Agregar 2.Editar 3.Eliminar 4.Mostrar Disponibles  +");
            Console.WriteLine("+  5.Buscar       6.Volver al menu inicial              +");
            Console.WriteLine("+-------------------------------------------------------+");
            Console.Write(">");
            String opcion2 = Console.ReadLine();
            switch (opcion2)
            {
                case "1":
                    ControladorEmpleado.getInstancia().AgregarEmpleado();
                    break;
                case "2":
                    ControladorEmpleado.getInstancia().EditarEmpleados();
                    break;
                case "3":
                    ControladorEmpleado.getInstancia().EliminarEmpleado();
                    break;
                case "4":
                    ControladorEmpleado.getInstancia().MostrarEmpleados();
                    break;
                case "5":
                    ControladorEmpleado.getInstancia().BuscadorEmpleado();
                    break;
                case "6":
                    MenuMain();
                    break;
            }
        }
        #endregion
    }
}

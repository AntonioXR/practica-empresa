﻿using System;
using PracticaEmpleados.Controller;
using PracticaEmpleados.Model;
namespace PracticaEmpleados.View
{
    public class ResumenAll
    {
        #region "Singleton"
        private static ResumenAll instancia;
        public static ResumenAll getInstancia()
        {
            return (instancia == null) ? (instancia = new ResumenAll()) : instancia ;
        }
        #endregion
        #region "Presentacion de Resumen"
        public void resumen() {
            Console.WriteLine("########## RESUMEN DE DATOS ##########");
            Console.WriteLine("\n--------------- DEPARTAMENTOS ---------------\n\tExisten "+ControladorDepartamento.getInstancia().Departamentos.Count+" departamentos disponibles");
            Console.WriteLine("\n------------------ PUESTOS ------------------\n\tExisten " + ControladorPuesto.getInstancia().Puesto.Count+ " puestos habiles");
            Console.WriteLine("\n---------------- EMPLEADOS  -----------------\n\tExisten " + ControladorEmpleado.getInstancia().Empleados.Count + " empleados ingresador");
            Console.WriteLine("\ny se detalla de la siguiente manera");
            foreach (Puesto pst in ControladorPuesto.getInstancia().Puesto)
            {
                Console.WriteLine("\nPuesto: " + pst.NombrePuesto);
                foreach (Empleado emp in ControladorEmpleado.getInstancia().Empleados) {
                    if (emp.PuestoEmpleado.IdPuesto == pst.IdPuesto) {
                        Console.WriteLine("\n+------------------------------------------+");
                        Console.WriteLine("Nombre: " + emp.NombreEmpleado + "\nApellido: " + emp.ApellidoEmpleado +"\nPuesto: " + emp.PuestoEmpleado.NombrePuesto + "\nDepartamento: " + emp.DepartamentoEmpleado.NombreDepartamento );
                        Console.WriteLine("+------------------------------------------+\n");
                    }
                }
            }
            Console.WriteLine("\n########## FIN uwu c:       ##########");
            Menus.getInstancia().MenuMain();
        }
        #endregion
    }
}

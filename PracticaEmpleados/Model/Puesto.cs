﻿using System;

namespace PracticaEmpleados.Model
{
    public class Puesto
    {
        #region "Propiedades"
        private int idPuesto = 0;
        private String nombrePuesto = "No disponible";
        #endregion
        #region "Constructores"
        public Puesto() { }
        public Puesto(int idPuesto, String nombrePuesto)
        {
            this.idPuesto = idPuesto;
            this.nombrePuesto = nombrePuesto;
        }
        #endregion
        #region "Getters/Setters"
        public int IdPuesto
        {
            get { return idPuesto; }
            set { this.idPuesto = value; }
        }
        public String NombrePuesto
        {
            get { return nombrePuesto; }
            set { this.nombrePuesto = value; }
        }
        #endregion
    }
}

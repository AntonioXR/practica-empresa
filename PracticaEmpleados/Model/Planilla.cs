﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaEmpleados.Model
{
    public class Planilla
    {
        #region "Datos planilla"
        public Empleado empleado { get; set; }
        public Double IGGS { get; set; }
        public Double Bonificacion { get; set; }
        public Double SeptDia { get; set; }
        public Double SalarioFinal { get; set; }

        #endregion
        #region "Constructores"
        public Planilla() {
        }
        public Planilla(Double iggs, Double bonificacion, Double sepDia, Double salario, Empleado empleado) {
            this.empleado = empleado;
            this.IGGS = iggs;
            this.SeptDia = sepDia;
            this.Bonificacion = bonificacion;
            this.SalarioFinal = salario;
        }
        #endregion
    }
}

﻿using System;

namespace PracticaEmpleados.Model
{
    public class Departamento
    {
        #region "Propiedades"
        private int idDepartamento = 0;
        private String nombreDepartamento = "No Disponible";
        #endregion
        #region "Constructores"
        public Departamento(){ }
        public Departamento(int idDepartamento) {
            IdDepartamento = idDepartamento;
        }
        public Departamento(int idDepartamento, String nombreDepartamento) {
            this.IdDepartamento = idDepartamento;
            this.NombreDepartamento = nombreDepartamento;
        }
        #endregion
        #region "Getters/Setters"
        public int IdDepartamento
        {
            get { return idDepartamento; }
            set { this.idDepartamento = value; }
        }
        public String NombreDepartamento
        {
            get { return nombreDepartamento; }
            set { this.nombreDepartamento = value; }
        }
        #endregion
    }
}

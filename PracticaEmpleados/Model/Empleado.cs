﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaEmpleados.Model
{
    public class Empleado
    {
        #region "Propiedades"
        private int idEmpleado = 0;
        private String nombreEmpleado = "No disponible";
        private String apellidoEmpleado = "No disponible";
        private String direccion = "No disponible";
        private String telefono = "No disponible";
        private double salario = 0.00;
        private Puesto puesto;
        private Departamento departamento;
        #endregion
        #region "Constructores"
        public Empleado() { }
        public Empleado(int idEmpleado, String nombre, String apellido, String direccion, String telefono, double salario, Puesto puesto, Departamento departamento)
        {
            this.IdEmpleado = idEmpleado;
            this.NombreEmpleado = nombre;
            this.ApellidoEmpleado = apellido;
            this.DireccionEmpleado = direccion;
            this.TelefonoEmpleado = telefono;
            this.SalarioEmpleado = salario;
            this.PuestoEmpleado = puesto;
            this.DepartamentoEmpleado = departamento;
        }
        #endregion
        #region "Getters/Setters"
        public int IdEmpleado
        {
            get { return idEmpleado; }
            set { this.idEmpleado = value; }
        }
        public String NombreEmpleado
        {
            get { return this.nombreEmpleado; }
            set { nombreEmpleado = value; }
        }
        public String ApellidoEmpleado
        {
            get { return this.apellidoEmpleado; }
            set { this.apellidoEmpleado = value; }
        }
        public String DireccionEmpleado
        {
            get { return this.direccion; }
            set { this.direccion = value; }
        }
        public String TelefonoEmpleado
        {
            get { return this.telefono; }
            set { this.telefono = value; }
        }
        public double SalarioEmpleado
        {
            get { return this.salario; }
            set { this.salario = value; }
        }
        public Departamento DepartamentoEmpleado
        {
            get { return departamento; }
            set { this.departamento = value; }
        }
        public Puesto PuestoEmpleado
        {
            get { return this.puesto; }
            set { this.puesto = value; }
        }
        #endregion
    }
}
